package com.example.concertapplication.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.concertapplication.data.Concert


@Dao
interface ConcertDao {

    @Query("SELECT * from concert ORDER BY `heart`")
    fun getConcerts(): LiveData<List<Concert>>

    @Query("SELECT * FROM concert WHERE id = :id")
    fun getConcertById(id: Long): Concert?


    @Insert
    suspend fun insert(game: Concert)

    @Insert
    suspend fun insert(game: List<Concert>)

    @Delete
    suspend fun delete(game: Concert)

    @Query("DELETE from concert")
    suspend fun deleteAll()

    @Update
    suspend fun updateConcert(concert: Concert)


}