package com.example.concertapplication.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.concertapplication.data.Concert


@Database(entities = [Concert::class], version = 3, exportSchema = false)
@TypeConverters(Converters::class)
abstract class ConcertRoomDatabase : RoomDatabase() {

    abstract fun concertDao(): ConcertDao

    companion object {
        private const val DATABASE_NAME = "CONCERT_DATABASE"

        @Volatile
        private var INSTANCE: ConcertRoomDatabase? = null

        fun getDatabase(context: Context): ConcertRoomDatabase? {
            if (INSTANCE == null) {
                synchronized(ConcertRoomDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            ConcertRoomDatabase::class.java, DATABASE_NAME
                        )
                            .fallbackToDestructiveMigration()
                            .build()
                    }
                }
            }
            return INSTANCE
        }
    }
}