@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.concertapplication


import android.graphics.Bitmap
import android.os.Bundle

import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.PaddingValues

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold

import androidx.compose.material3.Text

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember

import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData

import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.concertapplication.data.Concert

import com.example.concertapplication.ui.theme.ConcertApplicationTheme
import com.example.concertapplication.ui.theme.screens.AddConcertScreen
import com.example.concertapplication.ui.theme.screens.CameraCapture
import com.example.concertapplication.ui.theme.screens.ConcertScreen
import com.example.concertapplication.ui.theme.screens.Screen
import com.example.concertapplication.viewmodel.ConcertViewModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment


class MainActivity : ComponentActivity() {
    private var mGoogleMap:GoogleMap? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContent {
            ConcertApplicationTheme {
//                Surface(
//                    modifier = Modifier.fillMaxSize(),
//                ) {
//                    val navController = rememberNavController()
//                    NavHost(navController = navController, modifier = Modifier, inn)
//
//                }
                ConcertApplication()

            }

        }
    }




    @Composable
    fun ConcertApplication() {
        val navController = rememberNavController()
        Scaffold(

            bottomBar = {
                BottomNav(navController)
            }
        )
        { innerPadding ->
            NavHost(navController, modifier = Modifier, innerPadding)
        }
    }



    @Composable
    fun Greeting(name: String, modifier: Modifier = Modifier) {
        Text(
            text = "Hello $name!",
            modifier = modifier
        )
    }

    @Preview(showBackground = true)
    @Composable
    fun GreetingPreview() {
        ConcertApplicationTheme {
            Greeting("Android")
        }
    }

    @Composable
    fun BottomNav(navController: NavController) {
        NavigationBar(
            containerColor = Color(0xFF252525),
            modifier = Modifier.height(80.dp),

        ) {
            val navBackStackEntry by navController.currentBackStackEntryAsState()
            val currentDestination = navBackStackEntry?.destination
            val screens = listOf(
                Screen.ConcertScreen,
                Screen.AddConcertScreen,
            )
            screens.forEach { screen ->
                val isSelected = currentDestination?.hierarchy?.any { it.route == screen.route } == true

                NavigationBarItem(
                    selected = isSelected,

                    onClick = {
                        navController.navigate(screen.route) {

                            popUpTo(navController.graph.findStartDestination().id) {
                                saveState = true
                            }

                            launchSingleTop = true
                            restoreState = true
                        }
                    },
                    icon = {
                        val iconColor = if (isSelected) Color(0xFF945AD8) else Color(0xFF858585)

                        Image(
                            painter = painterResource(id = screen.customIconImage),
                            contentDescription = "kind of pet",
                            modifier = Modifier
                                .width(32.dp)
                                .height(30.dp)

                            ,
                            colorFilter = ColorFilter.tint(iconColor)

                        )
                    },
                    label = {


                        Text(
                            text = (screen.labelResourceId),
                            color = if (isSelected) Color(0xFF945AD8) else Color(0xFF858585)
                        )
                    }
                )
            }
        }
    }


    @Composable
    private fun NavHost(
        navController: NavHostController,
        modifier: Modifier,
        innerPadding: PaddingValues,


    ) {
        val viewModel: ConcertViewModel = viewModel()


        NavHost(
            navController,
            startDestination = Screen.ConcertScreen.route,
            modifier = modifier
        ) {
            composable(Screen.ConcertScreen.route) {
                ConcertScreen(navController, viewModel, delete = {
                    viewModel.deleteAllConcerts()
                })
            }
            composable(Screen.AddConcertScreen.route) {

                AddConcertScreen(
                    navController = navController,
                    viewModel = viewModel,
                    concert = { newConcert ->
                        viewModel.insertConcert(newConcert)
                    },

                )
            }
            composable(Screen.CameraCapture.route) {

                CameraCapture(
                    modifier = Modifier.fillMaxSize(), navController = navController
                )
            }


        }
            }
        }


