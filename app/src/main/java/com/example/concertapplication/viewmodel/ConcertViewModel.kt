package com.example.concertapplication.viewmodel

import android.app.Application
import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.concertapplication.data.Concert
import com.example.concertapplication.repository.ConcertRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ConcertViewModel(application: Application) : AndroidViewModel(application) {
    private val concertRepository = ConcertRepository(application.applicationContext)
    private val mainScope = CoroutineScope(Dispatchers.Main)


    val concertBacklog = concertRepository.getConcerts()
    private val _selectedConcert = MutableStateFlow<Concert?>(null)
    val selectedConcert: StateFlow<Concert?> = _selectedConcert



    private val _bitmaps = MutableStateFlow<List<Bitmap>>(emptyList())


    fun onTakePhoto(bitmap: Bitmap) {
        _bitmaps.value += bitmap
        Log.d("viewmodel", "Bitmap captured: $bitmap")

    }



    fun insertConcert(concert: Concert) {
        mainScope.launch {
            withContext(Dispatchers.IO) {
                concertRepository.insert(concert)
            }
        }
    }

    suspend fun getConcertById(id: Long): Concert? {
        return withContext(Dispatchers.IO) {
            concertRepository.getConcertById(id)
        }
    }



    fun deleteConcertBacklog(concert: Concert) {
        mainScope.launch {
            withContext(Dispatchers.IO) {
                concertRepository.delete(concert)
            }
        }
    }


    fun deleteConcert(concert: Concert) {
        mainScope.launch {
            withContext(Dispatchers.IO) {
                concertRepository.delete(concert)
            }
        }
    }

    fun deleteAllConcerts() {
        viewModelScope.launch {
            concertRepository.deleteAll()
        }
    }

    fun updateConcert(concert: Concert) {
        mainScope.launch {
            withContext(Dispatchers.IO) {
                concertRepository.updateConcert(concert)
            }
        }
    }

}
