package com.example.concertapplication.ui.theme.screens

import android.annotation.SuppressLint
import androidx.compose.ui.graphics.ImageBitmap
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Map
import androidx.compose.material.icons.filled.PhotoCamera
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import java.util.Date
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
//import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import com.example.concertapplication.R
import com.example.concertapplication.data.Concert
import com.example.concertapplication.utils.Utils
import com.example.concertapplication.viewmodel.ConcertViewModel
import java.io.ByteArrayOutputStream




@SuppressLint("UnrememberedMutableState")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddConcertScreen(
    navController: NavController, viewModel: ConcertViewModel, concert: (item: Concert) -> Unit,
    modifier: Modifier = Modifier,
    ) {


    val context = LocalContext.current

    val concertId: Long? =
        navController.previousBackStackEntry?.savedStateHandle?.get("concertId")
    var concertDetails: Concert? by remember { mutableStateOf(null) }




    var name by remember { mutableStateOf(concertDetails?.name ?: "") }
    var title by remember { mutableStateOf(concertDetails?.title ?: "") }
    var genre by remember { mutableStateOf(concertDetails?.genre ?: "") }
    var rating by remember { mutableStateOf(concertDetails?.rating ?: "") }
    var releaseDay by remember { mutableStateOf(concertDetails?.release?.day.toString() ?: "") }
    var releaseMonth by remember { mutableStateOf(concertDetails?.release?.month.toString() ?: "") }
    var releaseYear by remember { mutableStateOf(concertDetails?.release?.year.toString() ?: "") }
    var opinion by remember { mutableStateOf(concertDetails?.opinion ?: "") }

    val imageBitmap1: Bitmap? = navController.previousBackStackEntry?.savedStateHandle?.get("imageBitmap")


    var byteArray by remember { mutableStateOf<ByteArray?>(null) }

    if (imageBitmap1 != null) {
        byteArray = convertBitmapToByteArray(imageBitmap1)
    }




    var heartClicked by remember { mutableStateOf(concertDetails?.heart ?: false) }

    LaunchedEffect(key1 = concertId) {
        if (concertId != null) {
            concertDetails = viewModel.getConcertById(concertId)

            // Initialize state variables only if concert details are available
            concertDetails?.let { concert ->
                name = concert.name
                title = concert.title
                genre = concert.genre
                rating = concert.rating
                releaseDay = concert.release?.day?.toString() ?: ""
                releaseMonth = concert.release?.month?.toString() ?: ""
                releaseYear= concert.release?.year?.toString() ?: ""
                opinion = concert.opinion ?: ""
                byteArray = concert.bitmap
                heartClicked = concert.heart
            }
        }
    }





    Scaffold(
        containerColor = colorResource(id = R.color.gray),
        topBar = {
            TopAppBar(
                title = { Text(text = if (concertId != null) "Edit Concert" else "Add Concert", color = Color.White) },
                colors = TopAppBarDefaults.smallTopAppBarColors(colorResource(id = R.color.gray)))

        },
        content = { innerPadding ->
            Modifier.padding(innerPadding)
            Column(
                modifier = Modifier
                    .padding(top = 60.dp, start = 8.dp, end = 8.dp, bottom = 8.dp)
                    .fillMaxSize()
            ) {
                Card {
                    Column(
                        modifier = Modifier
                            .height(610.dp)
                            .width(400.dp)
                            .background(colorResource(id = R.color.lightgray))
                            .padding(top = 8.dp, start = 20.dp, end = 20.dp, bottom = 5.dp)


                    ) {

                        Row {


                            IconButton(onClick = { heartClicked = !heartClicked }) {
                                Icon(
                                    painter = if (heartClicked) painterResource(id = R.drawable.baseline_favorite_24) else painterResource(
                                        id = R.drawable.baseline_favorite_border_24
                                    ),
                                    contentDescription = "heart",
                                    tint = if (heartClicked) Color.Red else Color.White

                                )
                            }
                            Spacer(modifier = Modifier.weight(1f))
                            Button(
                                onClick = {
                                    navController.navigate(Screen.MapScreen.route)
                                },
                                modifier = Modifier
                                    .padding(top = 5.dp, bottom = 5.dp, end = 5.dp)
                                    .height(35.dp)
                                ,
                                colors = ButtonDefaults.buttonColors(
                                    containerColor = Color(0xFF945AD8),
                                    contentColor = Color.White
                                )
                            ) {
                                Icon(
                                    imageVector = Icons.Default.Map,
                                    contentDescription = "Take photo"
                                )
                            }

                        }
                        Box(
                            modifier = Modifier
                                .width(300.dp)
                                .height(200.dp)
                                .border(1.dp, Color.White, RoundedCornerShape(20.dp))
                                .align(Alignment.CenterHorizontally)
                        ) {
                            val imageBitmap: ImageBitmap? = byteArray?.let { bytes ->
                                BitmapFactory.decodeByteArray(bytes, 0, bytes.size)?.asImageBitmap()
                            }

                            Image(
                                bitmap = imageBitmap ?: ImageBitmap(
                                    1,
                                    1
                                ), // Provide a default ImageBitmap
                                contentDescription = null,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(200.dp)
                            )

                            Button(
                                onClick = {
                                    navController.navigate(Screen.CameraCapture.route)
                                },
                                modifier = Modifier
                                    .align(Alignment.BottomEnd)
                                    .padding(bottom = 16.dp, end = 16.dp),
                                colors = ButtonDefaults.buttonColors(
                                    containerColor = Color(0xFF945AD8),
                                    contentColor = Color.White
                                )
                            ) {
                                Icon(
                                    imageVector = Icons.Default.PhotoCamera,
                                    contentDescription = "Take photo"
                                )
                            }
                        }




                        Row(modifier = Modifier.fillMaxWidth()) {
                            CustomTextField(
                                value = name,
                                onValueChange = { name = it },
                                label = "Artist name",
                                keyboardType = KeyboardType.Text,
                                modifier = Modifier.weight(0.50f)

                            )
                            CustomTextField(
                                value = rating,
                                onValueChange = { rating = it },
                                label = "Rating",
                                keyboardType = KeyboardType.Text,
                                modifier = Modifier.weight(0.50f)
                            )
                        }

                        CustomTextField(
                            value = title,
                            onValueChange = { title = it },
                            label = "Tour name",
                            keyboardType = KeyboardType.Text,
                            modifier = Modifier.fillMaxWidth()
                        )



                        Row {
                            CustomTextField(
                                value = releaseDay,
                                onValueChange = { releaseDay = it },
                                label = "Day",
                                keyboardType = KeyboardType.Number,
                                modifier = Modifier.weight(0.33f)
                            )
                            CustomTextField(
                                value = releaseMonth,
                                onValueChange = { releaseMonth = it },
                                label = "Month",
                                keyboardType = KeyboardType.Number,
                                modifier = Modifier.weight(0.33f)
                            )
                            CustomTextField(
                                value = releaseYear,
                                onValueChange = { releaseYear = it },
                                label = "Year",
                                keyboardType = KeyboardType.Number,
                                modifier = Modifier.weight(0.33f)
                            )
                        }

                        CustomTextField(
                            value = genre,
                            onValueChange = { genre = it },
                            label = "Genre",
                            keyboardType = KeyboardType.Text,
                            modifier = Modifier.fillMaxWidth()
                        )

                        CustomTextField(
                            value = opinion,
                            onValueChange = { opinion = it },
                            label = "Opinion",
                            keyboardType = KeyboardType.Text,
                            modifier = Modifier.fillMaxWidth()
                        )

                        Row(
                            modifier = Modifier
                                .padding(15.dp)
                                .align(Alignment.CenterHorizontally)
                        ) {

                            Button(
                                modifier = Modifier.width(150.dp),
                                onClick = { navController.navigate(Screen.ConcertScreen.route) },
                                colors = ButtonDefaults.buttonColors(
                                    containerColor = Color(
                                        0xFF945AD8
                                    ), contentColor = Color.White
                                )


                            ) {
                                Text(text = "Cancel")
                            }

                            Spacer(modifier = Modifier.width(16.dp))

                            Button(
                                modifier = Modifier.width(150.dp),
                                onClick = {
                                    val releaseDate = Utils.dayMonthYearToDate(
                                        releaseDay.toInt(),
                                        releaseMonth.toInt(),
                                        releaseYear.toInt()
                                    )

                                    releaseDate?.let { release ->

                                        val newConcert = verifyInputAndCorrect(
                                            context,
                                            name,
                                            title,
                                            genre,
                                            rating,
                                            release,
                                            opinion,
                                            byteArray,
                                            concertDetails?.id,
                                            heartClicked
                                        )

                                        // Proceed with saving/updating concert
                                        if (concertId != null) {
                                            newConcert.let { viewModel.updateConcert(it) }
                                        } else {
                                            concert(newConcert)
                                        }
                                        navController.navigate(Screen.ConcertScreen.route)
                                    }
                                },
                                colors = ButtonDefaults.buttonColors(
                                    containerColor = Color(
                                        0xFF945AD8
                                    ), contentColor = Color.White
                                )
                            ) {
                                Text(text = "Save")
                            }


                        }
                    }

                }


            }
        })
}




@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun CustomTextField(
    value: String,
    onValueChange: (String) -> Unit,
    label: String,
    keyboardType: KeyboardType,
    modifier: Modifier = Modifier,

) {
    val customTextFieldStyle = TextStyle(
        color = Color.White
    )
    TextField(
        value = value,
        onValueChange = onValueChange,
        label = { Text(text = label) },
        textStyle = customTextFieldStyle,

        colors = TextFieldDefaults.textFieldColors(
            containerColor = Color.Transparent
        ),
        singleLine = true,
        modifier = modifier,
        keyboardOptions = KeyboardOptions(keyboardType = keyboardType)
    )
}

private fun verifyInputAndCorrect(
    context: Context, name: String, title: String, genre: String, rating: String, release: Date,
    opinion: String, byteArray: ByteArray?,
    existingConcertId: Long?, heartClicked: Boolean
): Concert {
//
    val concertId = existingConcertId ?: generateUniqueID()

    val resultingConcert = Concert(
        bitmap = byteArray ?: ByteArray(0),
        name = name,
        title = title,
        genre = genre,
        rating = rating,
        release = release,
        opinion = opinion,
        heart = heartClicked,
        id = concertId
    )

    Log.d("Concert", "Concert ID: $concertId")
    Log.d("Concert", "Bitmap size: ${byteArray?.size}")

    // Check if bitmap is not null before attempting to log it
    byteArray?.let { bitmapByteArray ->
        Log.d("Concert", "Bitmap saved successfully for concert ID: $concertId")
    } ?: run {
        Log.e("Concert", "Bitmap is null for concert ID: $concertId")
    }


    return resultingConcert
}


private fun generateUniqueID(): Long {
    return System.currentTimeMillis()
}

private fun convertBitmapToByteArray(bitmap: Bitmap): ByteArray {
    val stream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
    return stream.toByteArray()
}