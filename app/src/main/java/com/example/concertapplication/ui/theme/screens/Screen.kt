package com.example.concertapplication.ui.theme.screens

import androidx.annotation.StringRes
import com.example.concertapplication.R

sealed class Screen(
    val route: String,
    val labelResourceId: String,
    val customIconImage: Int
) {
    object ConcertScreen:
        Screen("concerts", "Concerts",R.drawable.baseline_music_note_24)
    object AddConcertScreen: Screen("add_concert_screen", "Add", R.drawable.baseline_add_box_24)
    object CameraCapture: Screen("setup_camera", "Camera", R.drawable.baseline_person_24 )

object MapScreen: Screen("map_screen", "Map", R.drawable.baseline_person_24)
}