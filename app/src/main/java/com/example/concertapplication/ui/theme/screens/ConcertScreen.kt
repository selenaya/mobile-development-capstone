package com.example.concertapplication.ui.theme.screens

import android.content.Context
import android.graphics.BitmapFactory
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.DismissDirection
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.SwipeToDismiss
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DeleteForever
import androidx.compose.material.icons.filled.Sort
import androidx.compose.material.rememberDismissState
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SnackbarResult
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.navigation.NavController
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.sp
import com.example.concertapplication.R
import com.example.concertapplication.data.Concert
import com.example.concertapplication.utils.Utils
import com.example.concertapplication.viewmodel.ConcertViewModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ConcertScreen(navController: NavController, viewModel: ConcertViewModel, delete: () -> Unit, ) {
    val context = LocalContext.current
    val concerts = viewModel.concertBacklog
    val snackbarHostState = remember { SnackbarHostState() }
    var deletedBacklog by remember { mutableStateOf(false)}
    var sortAscending by remember { mutableStateOf(true) }
    var dropdownExpanded by remember { mutableStateOf(false) }



    Scaffold(
        snackbarHost = { SnackbarHost(snackbarHostState) },
        containerColor = colorResource(id = R.color.gray),
        topBar = {
            TopAppBar(
                title = {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(text = stringResource(id = R.string.app_name), color = Color.White)
                    }
                },

                actions = {
                    IconButton(onClick = { dropdownExpanded = !dropdownExpanded }) {
                        Icon(
                            imageVector = Icons.Default.Sort,
                            contentDescription = "Sort",
                            tint = Color.White
                        )
                    }
                    IconButton(onClick = viewModel::deleteAllConcerts) {
                        Icon(
                            imageVector = Icons.Default.DeleteForever,
                            contentDescription = "Delete",
                            tint = Color.White
                        )
                    }

                    DropdownMenu(
                        expanded = dropdownExpanded,
                        onDismissRequest = { dropdownExpanded = false },
                        modifier = Modifier.align(Alignment.CenterVertically),
                    ) {
                        // Dropdown menu item for sorting
                        DropdownMenuItem(
                            onClick = {
                                sortAscending = !sortAscending
                                dropdownExpanded = false
                            }
                        ) {
                            Text("Favorite")
                        }
                    }
                },
                colors = TopAppBarDefaults.smallTopAppBarColors(colorResource(id = R.color.gray))
            )
        },

        content = { innerPadding ->
            Modifier.padding(innerPadding)

            if (!deletedBacklog) {
                Games(
                    context = context,
                    concerts,
                    modifier = Modifier.padding(16.dp),
                    viewModel,
                    sortAscending = sortAscending,
                    navController = navController,
                    snackbarHostState
                )
            }
        })


}

@Composable
fun Games(
    context: Context,
    concert: LiveData<List<Concert>>,
    modifier: Modifier,
    viewModel: ConcertViewModel,
    sortAscending: Boolean,
    navController: NavController,
    snackbarHostState: SnackbarHostState
) {
    val concertState by concert.observeAsState()

    LazyColumn(
        modifier = modifier
            .padding(top = 50.dp, start = 8.dp, end = 8.dp, bottom = 8.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        val sortedConcerts = concertState?.let { concerts ->
            if (sortAscending) {
                concerts.sortedBy { concert -> concert.release }
            } else {
                concerts.sortedByDescending { concert -> concert.release }
            }
        }

        sortedConcerts?.let { sortedConcertsList ->
            itemsIndexed(
                items = sortedConcertsList,
                key = { _, concert -> concert.hashCode() }
            ) { index, concert ->
                GameCard(
                    context,
                    concert = concert,
                    deleteGame = { viewModel.deleteConcert(concert) },
                    isLastItem = index == sortedConcertsList.size - 1,
                    navController = navController,
                    snackbarHostState = snackbarHostState
                )
            }
        }
    }
}



@OptIn(ExperimentalMaterialApi::class)
@Composable
fun GameCard(
    context: Context,
    concert: Concert,
    deleteGame: (Concert) -> Unit,
    isLastItem: Boolean,
    navController: NavController,
    snackbarHostState: SnackbarHostState
) {
    var heartClicked by remember { mutableStateOf(concert.heart) }
    val dismissState = rememberDismissState()
    if (dismissState.isDismissed(DismissDirection.StartToEnd) || dismissState.isDismissed(
            DismissDirection.EndToStart
        )
    ) {
        LaunchedEffect(Unit) {
            val result = snackbarHostState.showSnackbar(
                message = context.getString(R.string.deleted_game, concert.title),
                actionLabel = context.getString(R.string.undo),
                duration = SnackbarDuration.Long
            )
            if (result != SnackbarResult.ActionPerformed) {
                deleteGame(concert)
            } else {
                dismissState.reset()
            }
        }
    }
    Column {
        SwipeToDismiss(
            state = dismissState,
            background = {},
            dismissContent = {
                Card(
                    modifier = Modifier.clickable {
                        navController.currentBackStackEntry?.savedStateHandle?.set("concertId", concert.id)
                        navController.navigate(Screen.AddConcertScreen.route)
                    }
                ) {
                    val imageBitmap =
                        concert.bitmap.let { BitmapFactory.decodeByteArray(it, 0, it.size)?.asImageBitmap() }

                    Box(
                        modifier = Modifier
                            .height(250.dp)
                            .width(400.dp)
                            .background(colorResource(id = R.color.lightgray))
                    ) {
                        imageBitmap?.let {
                            Image(
                                bitmap = it,
                                contentDescription = null,
                                modifier = Modifier
                                    .height(250.dp)
                                    .fillMaxWidth()
                            )
                        }

                        Column(
                            modifier = Modifier
                                .height(250.dp)
                                .width(400.dp)
                        ) {
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(10.dp),
                                horizontalArrangement = Arrangement.End
                            ) {
                                IconButton(onClick = { /* No action when clicked in ConcertScreen */ }) {
                                    Icon(
                                        painter = if (heartClicked) painterResource(id = R.drawable.baseline_favorite_24) else painterResource(id = R.drawable.baseline_favorite_border_24),
                                        contentDescription = "heart",
                                        tint = if (heartClicked) Color.Red else Color.White
                                    )
                                }
                                Spacer(modifier = Modifier.weight(1f))
                                Card(
                                    modifier = Modifier
                                        .clip(RoundedCornerShape(20.dp))
                                ) {
                                    Text(
                                        text = Utils.dateToString2(concert.release),
                                        modifier = Modifier.padding(6.dp),
                                        color = Color.Black,
                                        fontSize = 12.sp
                                    )
                                }
                            }
                        }
                    }
                }
            },
            directions = setOf(DismissDirection.EndToStart, DismissDirection.StartToEnd),
        )

        Row {
            Column {
                Text(
                    text = concert.title ?: "",
                    color = Color.White
                )
                Text(
                    text = concert.name ?: "",
                    style = MaterialTheme.typography.headlineSmall,
                    fontStyle = FontStyle.Italic,
                    color = Color.White
                )
            }

            Spacer(modifier = Modifier.weight(1f))
            Text(
                text = "${concert.rating ?: ""}/10",
                color = Color.White
            )
            Image(
                painter = painterResource(id = R.drawable.baseline_star_24),
                contentDescription = "star",
                modifier = Modifier.size(20.dp)
            )
        }
        if (isLastItem) {
            Spacer(modifier = Modifier.padding(40.dp))
        }
    }
}
