package com.example.concertapplication.repository

import android.content.Context
import com.example.concertapplication.data.Concert
import com.example.concertapplication.db.ConcertDao
import com.example.concertapplication.db.ConcertRoomDatabase

class ConcertRepository(context: Context) {

    private val concertDao: ConcertDao

    init {
        val database = ConcertRoomDatabase.getDatabase(context)
        concertDao = database!!.concertDao()
    }

    suspend fun insert(concert: Concert) = concertDao.insert(concert)

    suspend fun updateConcert(concert: Concert) = concertDao.updateConcert(concert)
    suspend fun delete(concert: Concert) = concertDao.delete(concert)

    fun getConcerts() = concertDao.getConcerts()

    fun getConcertById(id: Long) = concertDao.getConcertById(id)


    suspend fun deleteAll() = concertDao.deleteAll()





}