package com.example.concertapplication.data



import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "concert")
data class Concert(


    @ColumnInfo(name = "bitmap")
    val bitmap: ByteArray,

    @ColumnInfo(name = "name")
    var name: String,

    @ColumnInfo(name = "title")
    var title: String,

    @ColumnInfo(name = "genre")
    var genre: String,

    @ColumnInfo(name = "rating")
    var rating:String,

    @ColumnInfo(name = "release")
    var release: Date,

    @ColumnInfo(name = "opinion")
    var opinion: String,

    @ColumnInfo(name = "heart")
    var heart: Boolean,

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0,

    )